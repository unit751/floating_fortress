using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BottomPanel : UIPanel
{
    protected override void OnAwake()
    {
        base.OnAwake();
        rectTransform.anchoredPosition = (rectTransform.rect.height / 2f) * Vector2.down;
    }

    protected override void OnStart()
    {
        base.OnStart();
        rectTransform.DOAnchorPosY(rectTransform.rect.height / 2f, 0.5f);
    }

    public override void Hide()
    {
        rectTransform.DOAnchorPosY(-rectTransform.rect.height / 2f, 0.5f).OnComplete(() =>
        {
            Destroy(gameObject);
        });
    }
}
