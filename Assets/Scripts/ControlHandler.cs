using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine;

public class ControlHandler : MonoBehaviour
{
    private UI ui;
    public ShipController shipController;
    public BattleHandler battleHandler;
    public PlayerController playerController;
    public JoystickController joystickController;
    public CameraController cameraController;

    public Vector3 Direction=>joystickController.direction;
    public float RotationAngle => joystickController.rotationAngle;
    
    public enum GameMode
    {
        ship,
        player,
        animation
    }
    public GameMode currentGameMode;

    private void Awake()
    {
        ui = FindObjectOfType<UI>();
        ui.changeViewButton.clickEvent.AddListener(ChangeGameMod);
    }

    private void Start()
    {
        if (currentGameMode==GameMode.ship) ChangeGameModeShip();
        else ChangeGameModePlayer();
    }

    public void ChangeGameMod()
    {
        if (currentGameMode == GameMode.animation) return;
        if (currentGameMode!=GameMode.ship) ChangeGameModeShip();
        else ChangeGameModePlayer();
    }

    private void ChangeGameModeShip()
    {
        currentGameMode = GameMode.animation;
        playerController.enabled = false;
        shipController.enabled = false;
        cameraController.ShipCamera(shipController.transform).OnComplete(() =>
        {
            shipController.enabled = true;
            cameraController.enabled = true;
            currentGameMode = GameMode.ship;
            battleHandler.StartBattle();
        });
    }
    
    private void ChangeGameModePlayer()
    {
        playerController.enabled = false;
        shipController.enabled = false;
        cameraController.PlayerCamera(playerController.transform,shipController.transform).OnComplete(() =>
        {
            playerController.enabled = true;
            cameraController.enabled = true;
            currentGameMode = GameMode.player;
            battleHandler.StopBattle();
        });
    }
}
