using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform _target;
    [SerializeField] private float smoothTime;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _offset;

    [SerializeField] private Vector3 playerViewOffset;
    [SerializeField] private Vector3 playerViewRotation;
    [SerializeField] private Vector3 shipViewOffset;
    [SerializeField] private Vector3 shipViewRotation;

    // Update is called once per frame
    void Update()
    {
        transform.position =
            Vector3.SmoothDamp(transform.position, _target.position + _offset, ref _velocity, smoothTime);
    }

    public Sequence ShipCamera(Transform shipTransform)
    {
        _target = shipTransform;
        _offset = shipViewOffset;
        var seq = DOTween.Sequence();
        seq.Append(transform.DOMove(_target.position + shipViewOffset, 1.5f));
        seq.Join(transform.DORotate(shipViewRotation, 1.5f));
        return seq;
    }

    public Sequence PlayerCamera(Transform playerTransform, Transform shipTransform)
    {
        _target = playerTransform;
        var seq = DOTween.Sequence();
        var targetPosition = shipTransform.TransformPoint(playerTransform.localPosition + playerViewOffset);
        _offset = targetPosition - playerTransform.position;
        var targetRotation = shipTransform.rotation.eulerAngles + playerViewRotation;
        seq.Append(transform.DOMove(targetPosition, 1.5f));
        seq.Join(transform.DORotate(targetRotation, 1.5f));
        return seq;
    }
}
