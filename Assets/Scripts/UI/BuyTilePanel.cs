using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuyTilePanel : BottomPanel
{
    public DisablableButton buyButton;
    public TextMeshProUGUI costText;

    [Header("Tile Data")] 
    public TileBuyTrigger buyTrigger;
    public Tile targetTile;

    private ResourceManager _resourceManager;

    protected override void OnStart()
    {
        base.OnStart();
        costText.text = targetTile.cost.ToString();
        buyButton.clickEvent.AddListener(Buy);
        _resourceManager = FindObjectOfType<ResourceManager>();
        CheckCost();
    }

    private bool CheckCost()
    {
        var enoughResources = targetTile.cost <= _resourceManager.GetResource(ResourceManager.ResourceType.wood);
        print(targetTile.cost + " " + _resourceManager.GetResource(ResourceManager.ResourceType.wood));
        buyButton.buttonEnabled = enoughResources;
        return enoughResources;
    }
    private void Buy()
    {
        if (!CheckCost()) return;
        _resourceManager.SpendResource(ResourceManager.ResourceType.wood, targetTile.cost);
        targetTile.ActivateTile();
        targetTile.SpawnAnimation();
        FindObjectOfType<Ship>().CheckAllTriggers();
        Hide();
    }
}
