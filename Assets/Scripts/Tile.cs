using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class Tile : MonoBehaviour
{
    public Vector2Int index;
    public bool active;
    public int cost;
    [SerializeField] private BoxCollider collider;
    [SerializeField] private Transform modelHolder;
    [SerializeField] private Transform buyTriggersHolder;
    [SerializeField] private List<TileBuyTrigger> buyTriggers;
    public BuyTilePanel buyTilePanelPrefab;
    [SerializeField] private ParticleSystem puffParticleSystem;

    public void ActivateTile(bool activate = true)
    {
        ChangeColliderHeight(activate ? 1f : 8f);
        modelHolder.gameObject.SetActive(activate);
        buyTriggersHolder.gameObject.SetActive(activate);
        active = activate;
    }

    public Sequence SpawnAnimation()
    {
        Instantiate(puffParticleSystem, transform);
        var seq = DOTween.Sequence();
        seq.Append(modelHolder.DOScale(1.1f, 0.2f));
        seq.Append(modelHolder.DOScale(1f, 0.1f));
        return seq;
    }

    private void ChangeColliderHeight(float newHeight)
    {
        var colliderSize = collider.size;
        colliderSize.y = newHeight;
        collider.size = colliderSize;
    }

    public void CheckTriggers(Dictionary<Vector2Int,Tile> tiles)
    {
        var triggersToDestroyList = new List<TileBuyTrigger>();
        var ui = FindObjectOfType<UI>();
        foreach (var trigger in buyTriggers)
        {
            if (!tiles.TryGetValue(index + trigger.direction, out var nTile)) triggersToDestroyList.Add(trigger);
            if (nTile != null && nTile.active) triggersToDestroyList.Add(trigger);
            else
            {
                trigger.targetTile = nTile;
                trigger.ui = ui;
            }
        }

        foreach (var trigger in triggersToDestroyList)
        {
            buyTriggers.Remove(trigger);
            Destroy(trigger.gameObject);
        }
    }
}
