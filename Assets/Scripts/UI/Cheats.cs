using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cheats : UIPanel
{
    private int _clicksCount = 10;
    private int _clicksCounter = 0;
    private ResourceManager _resourceManager;

    public void AddClickCounter()
    {
        _clicksCounter++;
        if (_clicksCounter<_clicksCount) return;
        _clicksCounter = 0;
        Show();
    }
    protected override void OnAwake()
    {
        base.OnAwake();
        _resourceManager = FindObjectOfType<ResourceManager>();
    }

    public void Restart()
    {
        DOTween.KillAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AddWood()
    {
        _resourceManager.AddResource(ResourceManager.ResourceType.wood, 1000);
    }

    public void AddFood()
    {
        _resourceManager.AddResource(ResourceManager.ResourceType.food, 1000);
    }
    
    public void ResetData()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        FindObjectOfType<ResourceManager>().ResetResources();
    }
}
