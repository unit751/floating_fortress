using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController characterController;
    [SerializeField] private Animator animator;
    [SerializeField] private ControlHandler controlHandler;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;

    // Update is called once per frame
    void Update()
    {
        var controllerDirection = controlHandler.shipController.transform.TransformDirection(controlHandler.Direction);
        characterController.Move(controllerDirection * speed);
        animator.SetFloat("X", controllerDirection.x);
        animator.SetFloat("Z", controllerDirection.z);
        transform.transform.rotation = Quaternion.Slerp(transform.rotation,
            controlHandler.shipController.transform.rotation *
            Quaternion.Euler(controlHandler.RotationAngle * Vector3.up), rotationSpeed * Time.deltaTime);
    }
}
