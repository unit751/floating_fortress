﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WinPanel : UIPanel
{

    public Image nextButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Show()
    {
        base.Show();

        //button
        nextButton.rectTransform.DOKill();
        nextButton.rectTransform.localScale = Vector3.one;
        nextButton.rectTransform.DOScale(1.2f, 0.8f).SetLoops(-1, LoopType.Yoyo);
    }

    public override void Hide()
    {
        base.Hide();

        //button
        nextButton.rectTransform.DOKill();
        nextButton.rectTransform.localScale = Vector3.one;
    }
}
