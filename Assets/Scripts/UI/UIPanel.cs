﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Serialization;

public class UIPanel : MonoBehaviour
{
   public RectTransform rectTransform;
    private void Awake()
    {
        OnAwake();
    }

    private void Start()
    {
        OnStart();
    }

    virtual protected void OnAwake()
    {
        
    }

    virtual protected void OnStart()
    {
        
    }
    virtual public void Show() {
        gameObject.SetActive(true);
    }

    virtual public void Hide() {
        gameObject.SetActive(false);
    }
}
