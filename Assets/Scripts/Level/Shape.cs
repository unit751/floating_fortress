using UnityEngine;

public class Shape
{
    public static void Flat(Mesh mesh, Mesh flat)
    {
        var v = mesh.vertices;
        var t = mesh.triangles;
        var c = mesh.colors;
        var uv = mesh.uv;

        var vertices = new Vector3[v.Length];
        for (int i = 0; i < vertices.Length; i++)
            vertices[i] = v[i];

        var triangles = new int[t.Length];
        for (int i = 0; i < triangles.Length; i++)
            triangles[i] = i;

        var colors = new Color[c.Length > 0 ? t.Length : 0];
        for (int i = 0; i < colors.Length / 3; i++)
            colors[3 * i] = colors[3 * i + 1] = colors[3 * i + 2] = c[t[3 * i + Random.Range(0, 3)]];

        flat.Clear();
        flat.vertices = vertices;
        flat.triangles = triangles;
        flat.colors = colors;
        flat.uv = uv;
    }

    public static void Loop(Mesh mesh, Vector3[] points, int edges, bool rectangular = true)
    {
        bool isFirstCenter = Mathf.Abs(points[0].x) < Mathf.Epsilon;
        bool isLastCenter = Mathf.Abs(points[points.Length - 1].x) < Mathf.Epsilon;

        var angleAdd = Quaternion.Euler(0, 180f / edges, 0);
        var x = Quaternion.Euler(0, 90f, 0) * Vector3.right;
        var y = Vector3.up;
        var z = Vector3.forward;
        var angle = Quaternion.Euler(0, -360f / edges, 0);
        int vFirst = isFirstCenter ? 1 : edges;
        int vMiddle = (points.Length - 2) * edges;
        int vLast = isLastCenter ? 1 : edges;

        var vertices = new Vector3[vFirst + vMiddle + vLast];
        int v = 0;
        if (isFirstCenter)
            vertices[v++] = y * points[0].y + z * points[0].z;
        for (int p = isFirstCenter ? 1 : 0; p < points.Length - (isLastCenter ? 1 : 0); p++)
        {
            for (int i = 0; i < edges; i++, x = angle * x)
                vertices[v++] = x * points[p].x + y * points[p].y + z * points[p].z;
            x = rectangular ? x : angleAdd * x;
        }

        if (isLastCenter)
            vertices[v++] = y * points[points.Length - 1].y + z * points[points.Length - 1].z;

        var triangles = new int[((isFirstCenter ? 3 : 6) + 6 * (points.Length - 3) + (isLastCenter ? 3 : 6)) * edges];
        int t = 0;
        if (isFirstCenter)
        {
            for (int i = 0, j = edges - 1; i < edges; j = i++)
            {
                triangles[t++] = 0;
                triangles[t++] = j + 1;
                triangles[t++] = i + 1;
            }
        }

        for (int i = 0, j = edges - 1; i < edges; j = i++)
        {
            for (int p = isFirstCenter ? 1 : 0; p < points.Length - (isLastCenter ? 2 : 1); p++)
            {
                triangles[t++] = i + p * edges + vFirst - edges;
                triangles[t++] = j + p * edges + vFirst - edges;
                triangles[t++] = i + (p + 1) * edges + vFirst - edges;
                triangles[t++] = i + (p + 1) * edges + vFirst - edges;
                triangles[t++] = j + p * edges + vFirst - edges;
                triangles[t++] = j + (p + 1) * edges + vFirst - edges;
            }
        }

        if (isLastCenter)
        {
            for (int i = 0, j = edges - 1; i < edges; j = i++)
            {
                triangles[t++] = vertices.Length - 2 - j;
                triangles[t++] = vertices.Length - 2 - i;
                triangles[t++] = vertices.Length - 1;
            }
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }

    public static void Scale(Mesh mesh, Vector3 scale)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
            vertices[i].Scale(scale);
        mesh.vertices = vertices;
    }

    public static void Translate(Mesh mesh, Vector3 move)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
            vertices[i] += move;
        mesh.vertices = vertices;
    }

    public static void Rotate(Mesh mesh, Quaternion rotate)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
            vertices[i] = rotate * vertices[i];
        mesh.vertices = vertices;
    }

    public static void Deform(Mesh mesh, Vector3 distance, Vector3 scale, Vector3 offset)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            var v = vertices[i] + offset;
            v.Scale(new Vector3(1f / scale.x, 1f / scale.y, 1f / scale.z));
            var d = new Vector3(Mathf.PerlinNoise(v.y, v.z), Mathf.PerlinNoise(v.x, v.z), Mathf.PerlinNoise(v.x, v.y)) -
                    Vector3.one / 2f;
            d.Scale(2 * distance);
            vertices[i] += d;
        }

        mesh.vertices = vertices;
    }

    public static void Paint(Mesh mesh, Gradient color, float value, float range)
    {
        var colors = new Color[mesh.vertexCount];
        for (int i = 0; i < mesh.vertexCount; i++)
            colors[i] = color.Evaluate(value + Random.Range(-range, range));
        mesh.colors = colors;
    }

    public static void Paint(Mesh mesh, Color color)
    {
        var colors = new Color[mesh.vertexCount];
        for (int i = 0; i < mesh.vertexCount; i++)
            colors[i] = color;
        mesh.colors = colors;
    }

    public static void Plane(Mesh mesh, Vector3 size, Vector2Int resolution, bool rectangular = true)
    {
        var vertices = new Vector3[(resolution.x + 1) * (resolution.y + 1)];
        var uvs = new Vector2[(resolution.x + 1) * (resolution.y + 1)];
        for (int i = 0; i <= resolution.x; i++)
        for (int j = 0; j <= resolution.y; j++)
        {
            vertices[i * (resolution.y + 1) + j] = new Vector3(size.x * i / resolution.x - size.x / 2f, 0,
                size.z * (j + (rectangular ? 0 : (i % 2) / 2f)) / resolution.y - size.z / 2f);
            uvs[i * (resolution.y + 1) + j] = new Vector2(1f * i / resolution.x,
                ((float)(j + (rectangular ? 0 : (i % 2) / 2f))) / resolution.y);
        }

        var triangles = new int[6 * resolution.x * resolution.y];
        for (int i = 0; i < resolution.x; i++)
        {
            for (int j = 0; j < resolution.y; j++)
            {
                int t = 6 * (i * resolution.y + j);
                triangles[t] = i * (resolution.y + 1) + j;
                triangles[t + 1] = i * (resolution.y + 1) + j + 1;
                triangles[t + 2] = (i + 1) * (resolution.y + 1) + j + i % 2;
                triangles[t + 3] = i * (resolution.y + 1) + j + (i + 1) % 2;
                triangles[t + 4] = (i + 1) * (resolution.y + 1) + j + 1;
                triangles[t + 5] = (i + 1) * (resolution.y + 1) + j;
            }
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
    }

    public static Vector3[] CurvePath(AnimationCurve x, AnimationCurve z, int count)
    {
        var p = new Vector3[count + 1];
        var dy = 1f / count;
        for (int i = 0; i < p.Length; i++)
            p[i] = new Vector3(x.Evaluate(i * dy), i * dy, z.Evaluate(i * dy));
        for (int l = 0; l < 5; l++)
        {
            for (int i = 1; i < p.Length - 1; i++)
            {
                var max = 0f;
                for (float t = 0f; t <= 1f; t += 0.1f)
                {
                    var y = Mathf.Lerp(p[i - 1].y, p[i + 1].y, t);
                    var diff = Mathf.Abs(x.Evaluate(y) - Mathf.Lerp(p[i - 1].x, p[i + 1].x, t))
                               + Mathf.Abs(z.Evaluate(y) - Mathf.Lerp(p[i - 1].z, p[i + 1].z, t));
                    if (diff > max)
                    {
                        max = diff;
                        p[i].y = y;
                        p[i].x = x.Evaluate(y);
                        p[i].z = z.Evaluate(y);
                    }
                }
            }
        }

        return p;
    }
}