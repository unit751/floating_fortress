using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorHandler : MonoBehaviour
{
    private ResourceManager _resourceManager;

    private void Awake()
    {
        _resourceManager = FindObjectOfType<ResourceManager>();
    }

    public void Collect(ResourceChunk resChunk)
    {
        _resourceManager.AddResource(resChunk.resType, resChunk.amount);
        resChunk.OnCollect();
    }
}
