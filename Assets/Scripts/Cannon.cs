using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public float maxFireDistance = 40f;
    public Vector3 targetPosition;
    public Transform cannonPlatform;
    public Transform cannon;
    public Transform cannonBallSpawnPoint;
    public CannonBall cannonBallPrefab;
    private Ship _ship;
    private readonly Vector3 _scaler = new Vector3(1f, 0, 1f);
    [SerializeField] private ParticleSystem smoke;
    [SerializeField] private ParticleSystem fire;
    public float fireCooldown;
    private float _timer=0f;

    private void Awake()
    {
        _ship = FindObjectOfType<Ship>();
        _ship.cannons.Add(this);
    }

    private void Update()
    {
        if (_timer < 0f)
        {
            //fire!
            Fire();
        }
        else
        {
            _timer -= Time.deltaTime;
        }
        if (!enabled) return;
        if (targetPosition==Vector3.zero) return;
        cannon.LookAt(targetPosition);
    }

    public void Fire()
    {
        if (!enabled) return;
        var cannonBall = Instantiate(cannonBallPrefab, cannonBallSpawnPoint.position, cannonBallSpawnPoint.rotation);
        cannonBall.Translate(targetPosition);
        _timer = fireCooldown;
        smoke.Play();
    }

}
