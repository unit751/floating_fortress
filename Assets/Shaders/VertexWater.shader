﻿Shader "Vertex/Water"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1, 1, 1)
        _Smoothness("Smoothness", Range(0, 1)) = 0
        _Metallic("Metallic", Range(0, 1)) = 0
        _Alpha("Alpha", Range(0, 1)) = 0
		_Scale("Scale", Vector) = (100, 10, 1)
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }

        Cull Off
		
        CGPROGRAM

        #pragma surface surf Standard nolightmap addshadow vertex:vert alpha
        #pragma target 3.0

        struct Input
        {
            float facing : VFACE;
			float4 vertexColor : COLOR;
        	float2 uv_MainTex;
        };

        sampler2D _MainTex;        

        half3 _Color;
        half _Smoothness;
        half _Metallic;
        half _Alpha;
		float4 _Scale;

        void vert(inout appdata_full v)
        {
		    float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
			float x = worldPos.x/_Scale.x;
			float z = worldPos.z/_Scale.z;
			float anim = (sin(x + _Time.y/_Scale.z) + sin(z/3 + _Time.y/_Scale.z/3) + sin(z/7 + _Time.y/_Scale.z/7)) / 3;
			worldPos.x += _Scale.y * anim;
			worldPos.y += _Scale.y * anim / 5;
			worldPos.z += _Scale.y * anim;
			v.vertex = mul(unity_WorldToObject, worldPos);
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
        	fixed4 col = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = col * _Color;
            o.Metallic = _Metallic;
            o.Smoothness = _Smoothness;
            o.Alpha = _Alpha;
        }

        ENDCG
    }
}
