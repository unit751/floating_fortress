using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data
{
    private const string Version = "0.1";
    
    private static Dictionary<string, int> _ValuesIntCache;
    private static Dictionary<string, float> _ValuesFloatCache;
    private static Dictionary<string, string> _ValuesStringCache;
    
    private static int Get(string key, int defaultValue)
    {
        if (_ValuesIntCache == null)
            _ValuesIntCache = new Dictionary<string, int>();
        if (_ValuesIntCache.TryGetValue(key, out int result))
            return result;
        result = PlayerPrefs.GetInt(Version + "_" + key, defaultValue);
        _ValuesIntCache.Add(key, result);
        return result;
    }

    private static void Set(string key, int value)
    {
        if (_ValuesIntCache == null)
            _ValuesIntCache = new Dictionary<string, int>();
        _ValuesIntCache[key] = value;
        PlayerPrefs.SetInt(Version + "_" + key, value);
    }
    
    private static float Get(string key, float defaultValue)
    {
        if (_ValuesFloatCache == null)
            _ValuesFloatCache = new Dictionary<string, float>();
        if (_ValuesFloatCache.TryGetValue(key, out var result))
            return result;
        result = PlayerPrefs.GetFloat(Version + "_" + key, defaultValue);
        _ValuesFloatCache.Add(key, result);
        return result;
    }

    private static void Set(string key, float value)
    {
        if (_ValuesFloatCache == null)
            _ValuesFloatCache = new Dictionary<string, float>();
        _ValuesFloatCache[key] = value;
        PlayerPrefs.SetFloat(Version + "_" + key, value);
    }

    private static string Get(string key, string defaultValue)
    {
        if (_ValuesStringCache == null)
            _ValuesStringCache = new Dictionary<string, string>();
        if (_ValuesStringCache.TryGetValue(key, out var result))
            return result;
        result = PlayerPrefs.GetString(Version + "_" + key, defaultValue);
        _ValuesStringCache.Add(key, result);
        return result;
    }

    private static void Set(string key, string value)
    {
        if (_ValuesStringCache == null)
            _ValuesStringCache = new Dictionary<string, string>();
        _ValuesStringCache[key] = value;
        PlayerPrefs.SetString(Version + "_" + key, value);
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }
    
    private const string ResourceKey = "_amount";

    public static int GetResource(ResourceManager.ResourceType resourceType)
    {
        return Get(resourceType+"_amount", 0);
    }

    public static void SetResource(ResourceManager.ResourceType resourceType, int value)
    {
        Set(resourceType+"_amount", value);
    }
}
