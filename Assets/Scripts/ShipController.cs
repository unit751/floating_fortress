using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    public ControlHandler controlHandler;
    public CharacterController characterController;

    void Update()
    {
        var controllerDirection = controlHandler.Direction;
        characterController.Move(controllerDirection * speed);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.Euler(controlHandler.RotationAngle * Vector3.up), rotationSpeed * Time.deltaTime);
        
        var transformPosition = transform.position;
        transformPosition.y = 0;
        transform.position = transformPosition;
    }

    private void OnEnable()
    {
        characterController.enabled = true;
    }

    private void OnDisable()
    {
        characterController.enabled = false;
    }
}
