using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TileBuyTrigger : MonoBehaviour
{
    public Vector2Int direction;
    public Tile targetTile;
    public Tile parentTile;
    private BuyTilePanel _buyTilePanel;
    public UI ui;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out PlayerController playerController))
        {
            _buyTilePanel = ui.ShowBuyTilePanel(targetTile.buyTilePanelPrefab, targetTile, this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out PlayerController playerController))
        {
            if (_buyTilePanel) _buyTilePanel.Hide();
        }
    }
}
