﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoystickController : MonoBehaviour
{
    public Image joybg;
    public Image joy;

    private Vector3 centerPos;
    private Vector3 delta;
    public Vector3 direction;
    public float rotationAngle=0f;
    // Start is called before the first frame update
    void Start()
    {
        joybg.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            StartMoving();            
        }
        if (Input.GetMouseButton(0)) {
            if (!joybg.IsActive()) StartMoving();

            delta = Input.mousePosition - centerPos;
            if (delta.magnitude > 150f) delta = delta.normalized * 150f;
            joy.rectTransform.anchoredPosition = delta;
            rotationAngle = Vector3.Angle(Vector3.up, delta) * Mathf.Sign(direction.x);

            direction = delta / 150f;
            direction = new Vector3(direction.x, 0f, direction.y);
        }
        if (Input.GetMouseButtonUp(0)) {
            direction = Vector3.zero;
            joybg.gameObject.SetActive(false);
        }
    }

    void StartMoving() {
        joybg.gameObject.SetActive(true);
        centerPos = Input.mousePosition;
        joybg.rectTransform.anchoredPosition = centerPos;
    }

    private void OnDisable()
    {
        joybg.gameObject.SetActive(false);
    }
}
