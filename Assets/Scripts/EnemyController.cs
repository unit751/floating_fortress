using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class EnemyController : MonoBehaviour
{
    public Vector3 dir;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    public Transform grinderModel;
    [SerializeField] private float grinderRotationSpeed;
    [SerializeField] private CharacterController characterController;
    [SerializeField] private int hp = 200;
    [SerializeField] private ResourceChunk resourceChunkPrefab;
    [SerializeField] private Transform tileHolder;

    public List<Renderer> renderers = new List<Renderer>();
    public List<Material> _materials = new List<Material>();
    public List<Color> _baseColors = new List<Color>();

    // Start is called before the first frame update
    void Start()
    {
        _takingDamageSeq = DOTween.Sequence();
        
        //create materials list
        renderers.AddRange(GetComponentsInChildren<Renderer>());
        var materials = new List<Material>();
        foreach (var rend in renderers)
        {
            materials.AddRange(rend.sharedMaterials);
        }
        var materialsNoDupes = materials.Distinct().ToList();
        foreach (var mat in materialsNoDupes)
        {
            _materials.Add(new Material(mat));
            _baseColors.Add(mat.color);
        }
        //change materials in-game
        foreach (var rend in renderers)
        {
            for (var i = 0; i < rend.sharedMaterials.Length; i++)
            {
                var j = materialsNoDupes.IndexOf(rend.sharedMaterials[i]);
                print(rend.gameObject.name + " " + rend.sharedMaterials[i].name + " " + i + " " + j);
                
                try
                {
                    rend.materials[i] = _materials[j];
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
        //test
        TakeDamage(0);
    }

    // Update is called once per frame
    private void Update()
    {
        grinderModel.Rotate(grinderRotationSpeed * Time.deltaTime * Vector3.left, Space.Self);
        characterController.Move(dir * speed);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.LookRotation(dir, Vector3.up), rotationSpeed * Time.deltaTime);
    }

    public void TakeDamage(int damage)
    {
        var resourceDrop = Mathf.Min(hp, damage);
        if(!_takingDamageSeq.active) _takingDamageSeq = TakingDamage();
        hp -= resourceDrop;
        var chunkCount = resourceDrop / 10;
        for (var i = 0; i < chunkCount; i++)
        {
            var chunk = Instantiate(resourceChunkPrefab, transform.position, Quaternion.identity);
            var targetPosition = transform.position + 3f * Random.insideUnitSphere;
            targetPosition = new Vector3(targetPosition.x, Mathf.Abs(targetPosition.y) + 2f, targetPosition.z);
            chunk.transform.position = targetPosition;
            chunk.transform.rotation = Quaternion.Euler(90f*Random.insideUnitSphere);
            
            chunk.rigidbody.AddExplosionForce(Random.Range(10f,25f), transform.position,10f,2f);
            chunk.rigidbody.AddTorque(40f * Random.insideUnitSphere, ForceMode.Force);
        }

        if (hp <= 0)
        {
            //death
            Destroyed();
        }
    }

    private void Destroyed()
    {
        onDeath.Invoke(this);
        var colliders = GetComponentsInChildren<Collider>();
        foreach (var collider in colliders)
        {
            Destroy(collider);
        }
        enabled = false;
        transform.DOBlendableMoveBy(5f * Vector3.down, 3f).OnComplete(() =>
        {
            Destroy(gameObject);
        });
    }
    public UnityEvent<EnemyController> onDeath;

    private Sequence _takingDamageSeq;

    private Sequence TakingDamage()
    {
        var colorFadeTime = 0.8f;
        var seq = DOTween.Sequence();
        seq.Append(tileHolder.DOShakePosition(2f * colorFadeTime, 1f, 8));
        foreach (var renderer in renderers)
        {
            var baseColor = renderer.material.color;
            seq.Insert(0f, ChangeColorSequence(renderer.material, baseColor, Color.red, colorFadeTime));
        }
        return seq;
    }

    private Sequence ChangeColorSequence(Material mat, Color startColor, Color changeColor, float animationTime)
    {
        var seq = DOTween.Sequence();
        seq.Append(mat.DOColor(changeColor, animationTime));
        seq.Append(mat.DOColor(startColor, animationTime));
        return seq;
    }
}
