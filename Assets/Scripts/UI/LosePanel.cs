﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class LosePanel : UIPanel
{
    public Image restartButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Show()
    {
        base.Show();

        //button
        restartButton.rectTransform.DOKill();
        restartButton.rectTransform.localScale = Vector3.one;
        restartButton.rectTransform.DOScale(1.2f, 0.4f).SetLoops(-1, LoopType.Yoyo);
    }

    public override void Hide()
    {
        base.Hide();

        //button
        restartButton.rectTransform.DOKill();
        restartButton.rectTransform.localScale = Vector3.one;
    }
}
