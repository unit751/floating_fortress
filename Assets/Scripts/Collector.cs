using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private CollectorHandler _collectorHandler;
    [SerializeField] private Transform grinderModel;
    [SerializeField] private float grinderRotationSpeed;
    
    [SerializeField] private ParticleSystem woodBurst;
    [Header("Jaws")] [SerializeField] private int jawDamage = 50;
    public Transform leftJaw;
    public Transform rightJaw;
    [SerializeField] private Transform boxCollider;
    [SerializeField] private LayerMask chewingMask;
    private Sequence chewingSeq;
    private void StartChewing()
    {
        chewingSeq = Chewing().SetLoops(-1, LoopType.Yoyo);
    }

    private Sequence Chewing()
    {
        var seq = DOTween.Sequence();

        seq.Append(leftJaw.DOBlendableRotateBy(30f * Vector3.up, 1f));
        seq.Join( rightJaw.DOBlendableRotateBy(-30f * Vector3.up, 1f));
        seq.AppendCallback(() =>
        {
            var hits = Physics.BoxCastAll(boxCollider.position, boxCollider.localScale / 2f, boxCollider.forward,
                boxCollider.rotation, 1f, chewingMask);
            foreach (var rHit in hits)
            {
                if (rHit.collider.TryGetComponent(out EnemyController enemy))
                {
                    enemy.TakeDamage(jawDamage);
                    break;
                }
            }
        });
        seq.Append(leftJaw.DOBlendableRotateBy(-30f * Vector3.up, 1f));
        seq.Join( rightJaw.DOBlendableRotateBy(30f * Vector3.up, 1f));

        return seq;
    }
    private void Awake()
    {
        _collectorHandler = FindObjectOfType<CollectorHandler>();
    }

    private void Start()
    {
        StartChewing();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.TryGetComponent(out ResourceChunk resourceChunk))
        {
            CollectResourceChunk(resourceChunk);
        }
    }

    private void CollectResourceChunk(ResourceChunk resourceChunk)
    {
        Destroy(resourceChunk.rigidbody);
        Destroy(resourceChunk.collider);
        resourceChunk.transform.parent = transform;
        resourceChunk.transform.DOLocalMove(grinderModel.transform.localPosition, 0.5f).SetEase(Ease.InCubic)
            .OnComplete(() =>
            {
                woodBurst.Emit(40);
                _collectorHandler.Collect(resourceChunk);
                resourceChunk.OnCollect();
            });
    }

    private void Update()
    {
        grinderModel.Rotate(grinderRotationSpeed * Time.deltaTime * Vector3.left, Space.Self);
    }

    private void OnDestroy()
    {
        chewingSeq.Kill();
    }
}
