﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;


public class Coins : MonoBehaviour
{
    public Image coinPrefab;
    public Image coinImage;
    public TextMeshProUGUI coinText;
    public int coins = 0;

    public RectTransform canvas;
    // Start is called before the first frame update
    void Start()
    {
        AddCoins(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AddCoins(int c) {
        coins += c;
        coinText.text = coins.ToString();
    }

    public void SpawnCoins(Vector3 spawnPoint, float maxCoinBurstDistance = 150f)
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(spawnPoint);
        Vector2 WorldObject_ScreenPosition = new Vector2(
            ((ViewportPosition.x * canvas.sizeDelta.x) - (canvas.sizeDelta.x * 0.5f)),
            ((ViewportPosition.y * canvas.sizeDelta.y) - (canvas.sizeDelta.y * 0.5f)));
        print(WorldObject_ScreenPosition);

        for (int i = 0; i < 5; i++)
        {
            var newCoin = Instantiate(coinPrefab, canvas.transform);
            newCoin.rectTransform.anchoredPosition = WorldObject_ScreenPosition;
            var seq = DOTween.Sequence();
            seq.Append(newCoin.rectTransform.DOAnchorPos(newCoin.rectTransform.anchoredPosition + Random.insideUnitCircle * maxCoinBurstDistance, 0.5f + Random.Range(0.1f, 0.2f)));
            seq.AppendInterval(Random.Range(0.1f, 0.2f));
            seq.Append(newCoin.rectTransform.DOMove(coinImage.rectTransform.position, 0.5f + Random.Range(0.1f, 0.2f)));
            seq.AppendCallback(() =>
            {
                AddCoins(25);
            });
            seq.SetUpdate(true);
        }
    }
}
