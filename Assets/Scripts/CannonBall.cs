using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public float speed;
    public int damage = 20;
    public ParticleSystem splash;
    public ParticleSystem explosion;

    public void Translate(Vector3 target)
    {
        transform.DOMove(target, Vector3.Distance(target, transform.position) / speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            if (other.TryGetComponent(out EnemyController enemy))
            {
                enemy.TakeDamage(damage);
            }
        }
        if (other.tag == "Water")
        {
            Instantiate(splash, transform.position, Quaternion.identity);
        }
        DOTween.Kill(transform);
        Destroy(gameObject);
    }
}
