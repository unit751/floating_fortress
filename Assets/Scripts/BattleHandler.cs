using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BattleHandler : MonoBehaviour
{
    public Ship ship;
    public EnemyController enemyPrefab;
    public List<EnemyController> enemies;
    public EnemyController closestEnemy;
    public int enemyCount = 8;
    

    // Start is called before the first frame update
    void Start()
    {
        enemies.AddRange(FindObjectsOfType<EnemyController>());
        foreach (var enemy in enemies)
        {
            enemy.onDeath.AddListener(OnEnemyDestroyed);
        }
    }

    private void Update()
    {
        closestEnemy = FindClosestEnemy();
        foreach (var enemy in enemies)
        {
            enemy.dir = (ship.transform.position - enemy.transform.position).normalized;
        }

        foreach (var cannon in ship.cannons)
        {
            if (!closestEnemy)
            {
                cannon.targetPosition = Vector3.zero;
                cannon.enabled = false;
                continue;
            }

            if (Vector3.Distance(cannon.transform.position, closestEnemy.transform.position) < cannon.maxFireDistance)
            {
                cannon.targetPosition = closestEnemy.transform.position;
                cannon.enabled = true;
            }
            else
            {
                cannon.targetPosition = Vector3.zero;;
                cannon.enabled = false;
            }
        }
    }

    private EnemyController FindClosestEnemy()
    {
        if (enemies.Count == 0)
        {
            SpawnEnemies(enemyCount);
            return FindClosestEnemy();
        }
        var closestEnemy = enemies[0];
        var distance = Vector3.Distance(ship.transform.position, closestEnemy.transform.position);
        foreach (var enemy in enemies)
        {
            var d = Vector3.Distance(ship.transform.position, enemy.transform.position);
            if ( d< distance)
            {
                d = distance;
                closestEnemy = enemy;
            }
        }
        return closestEnemy;
    }

    private void SpawnEnemies(int count)
    {
        var currentEnemiesCount = enemies.Count;
        for (var i = currentEnemiesCount; i < count; i++)
        {
            var r = Random.insideUnitCircle.normalized;
            var spawnPosition = new Vector3(r.x, 0f, r.y) * 200f;
            var newEnemy = Instantiate(enemyPrefab, ship.transform.position + spawnPosition, Quaternion.identity);
            enemies.Add(newEnemy);
            newEnemy.onDeath.AddListener(OnEnemyDestroyed);
        }
    }
    public void StartBattle()
    {
        foreach (var enemy in enemies)
        {
            enemy.enabled = true;
        }
        foreach (var cannon in ship.cannons)
        {
            cannon.enabled = true;
        }
        enabled = true;
    }

    public void StopBattle()
    {
        foreach (var enemy in enemies)
        {
            enemy.enabled = false;
            enemy.dir=Vector3.zero;
        }
        foreach (var cannon in ship.cannons)
        {
            cannon.enabled = false;
        }
        enabled = false;
    }

    private void OnEnemyDestroyed(EnemyController enemy)
    {
        enemies.Remove(enemy);
    }
}
