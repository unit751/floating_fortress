using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ResourceManager : MonoBehaviour
{

    [SerializeField] private bool startResourcesOverride;
    [SerializeField] private int startResources;
    private void Awake()
    {
        if (startResourcesOverride) SetResource(ResourceType.wood, startResources);
        CheckResources();
    }

    public void ResetResources()
    {
        SetResource(ResourceType.wood,0);
        SetResource(ResourceType.food,0);
    }

    private void CheckResources()
    {
        SetResource(ResourceType.wood, Data.GetResource(ResourceType.wood));
        SetResource(ResourceType.food, Data.GetResource(ResourceType.food));
    }

    public enum ResourceType
    {
        wood,
        food
    }

    private Dictionary<ResourceType, int> ResourcesCount = new Dictionary<ResourceType, int>();

    public void AddResource(ResourceType resType, int addCount)
    {
        SetResource(resType,ResourcesCount[resType] + addCount);
    }

    public void SpendResource(ResourceType resType, int spendCount)
    {
        SetResource(resType,ResourcesCount[resType] - spendCount);
    }

    private void SetResource(ResourceType resType, int setCount)
    {
        ResourcesCount[resType] = setCount;
        Data.SetResource(resType,ResourcesCount[resType]);
        
        onResourceChanged.Invoke();
    }
    
    public UnityEvent onResourceChanged;

    public int GetResource(ResourceType resType)
    {
        return ResourcesCount[resType];
    }
}
