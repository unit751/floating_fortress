using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DisablableButton : MonoBehaviour
{
    [SerializeField] private Image buttonImage;
    [SerializeField] private Sprite enabledSprite;
    [SerializeField] private Sprite disabledSprite;

    public bool buttonEnabled
    {
        get => _enabled;
        set
        {
            buttonImage.sprite = value ? enabledSprite : disabledSprite;
            _enabled = value;
        }
    }

    private bool _enabled = true;
    
    public UnityEvent clickEvent = new UnityEvent();

    private void Awake()
    {
        clickEvent.AddListener(OnClick);
    }

    private void OnClick()
    {
        
    }

    private void OnDestroy()
    {
        clickEvent.RemoveAllListeners();
    }

    public void Click()
    {
        clickEvent.Invoke();
    }
}
