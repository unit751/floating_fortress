using UnityEngine;

public class WaterControl : MonoBehaviour
{
    public Transform water1;
    [SerializeField] private float speed = 4;
    private Material _firstWaterMatOnStart;

    private float _speed;

    private void Start()
    {
        var meshRender = water1.GetComponent<WaterGround>().terrain.gameObject.GetComponent<MeshRenderer>();
        _firstWaterMatOnStart = meshRender.material;
        MoveForward();
    }

    private void Update()
    {
        var offset = _firstWaterMatOnStart.mainTextureOffset;
        offset += Vector2.right * (_speed * Time.deltaTime);
        offset.x %= 500;
        _firstWaterMatOnStart.mainTextureOffset = offset;
    }

    public void MoveForward()
    {
        _speed = speed;
    }

    public void MoveBack()
    {
        _speed = -speed;
    }

    public void Stop()
    {
        _speed = 0;
    }
}