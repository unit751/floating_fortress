using UnityEditor;
using UnityEngine;

public class WaterGround : MonoBehaviour
{
    public Vector3 size;
    public float meshStep = 1f;
    public Vector3 deform = Vector3.one / 5f;

    public Vector3 deformScale = Vector3.one;

    public MeshFilter terrain;

    private Vector3 _deformOffset;

#if UNITY_EDITOR
    [ContextMenu("Generate")]
    public void GeneratingTerrain()
    {
        terrain.mesh = new Mesh();
        _deformOffset = Random.Range(1f, 100f) * size;
        var t = terrain;
        var resolution = new Vector2Int((int) (size.x / meshStep), (int) (size.z / meshStep));
        Shape.Plane(t.mesh, size, resolution, false);
        Shape.Deform(t.mesh, deform, deformScale, t.transform.position + _deformOffset);
        t.mesh.RecalculateBounds();
        t.mesh.RecalculateNormals();
        t.GetComponent<MeshCollider>().sharedMesh = t.mesh;
        foreach (Transform child in t.transform)
            Destroy(child.gameObject);
        AssetDatabase.CreateAsset( t.mesh, "Assets/Mesh/water.asset");
        AssetDatabase.SaveAssets();
    }
#endif
}