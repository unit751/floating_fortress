﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI : MonoBehaviour
{
    private ResourceManager _resourceManager;
    private void Start()
    {
        _resourceManager = FindObjectOfType<ResourceManager>();
        _resourceManager.onResourceChanged.AddListener(UpdateResourceCounters);
        UpdateResourceCounters();
    }

    private void UpdateResourceCounters()
    {
        woodCounter.text = _resourceManager.GetResource(ResourceManager.ResourceType.wood).ToString();
        foodCounter.text = _resourceManager.GetResource(ResourceManager.ResourceType.food).ToString();
    }

    public DisablableButton changeViewButton;

    public BuyTilePanel ShowBuyTilePanel(BuyTilePanel prefab, Tile targetTile, TileBuyTrigger trigger)
    {
        var panel = Instantiate(prefab, transform);
        panel.targetTile = targetTile;
        panel.buyTrigger = trigger;
        panel.Show();
        return panel;
    }

    public TextMeshProUGUI woodCounter;
    public TextMeshProUGUI foodCounter;
}
