using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ship : MonoBehaviour
{
    [SerializeField]private Transform tileHolder;
    private UI ui;
    public Dictionary<Vector2Int, Tile> Tiles
    {
        get
        {
            if (_tiles.Count==0) GetTiles();
            return _tiles;
        }
    }
    private Dictionary<Vector2Int, Tile> _tiles = new Dictionary<Vector2Int, Tile>();
    public Transform wallsHolder;
    public Transform wallPrefab;
    public List<Cannon> cannons;
    private void Awake()
    {
        ui = FindObjectOfType<UI>();
        ui.changeViewButton.clickEvent.AddListener(ChangeView);
    }
    private void ChangeView()
    {
        print("Change view");
    }

    void Start()
    {
        ActivateTiles();
        CreateWalls();
    }

    private void GetTiles()
    {
        var allTiles = new List<Tile>();
        allTiles.AddRange(tileHolder.GetComponentsInChildren<Tile>());
        var newDictionary = new Dictionary<Vector2Int, Tile>();
        foreach (var tile in allTiles)
        {
            newDictionary.Add(tile.index,tile);
        }

        _tiles = newDictionary;
    }

    private void ActivateTiles()
    {
        foreach (var tile in Tiles)
        {
            tile.Value.ActivateTile(tile.Value.active); 
        }
        CheckAllTriggers();
    }

    public void CheckAllTriggers()
    {
        foreach (var tile in Tiles)
        {
            if (tile.Value.active) tile.Value.CheckTriggers(Tiles);
        }
    }

    private void CreateWalls()
    {
        var wallIndexes = new List<Vector2Int>();
        foreach (var index in Tiles.Keys)
        {
            for (var i = -1; i <= 1; i++)
            {
                for (var j = -1; j <= 1; j++)
                {
                    var checkIndex = index + new Vector2Int(i, j);
                    if(Tiles.ContainsKey(checkIndex)) continue;
                    if(wallIndexes.Contains(checkIndex)) continue;
                    wallIndexes.Add(checkIndex);
                }
            }
            
        }
        foreach (var index in wallIndexes)
        {
            Instantiate(wallPrefab, wallsHolder);
            wallPrefab.localPosition = LocalPositionByIndex(index);
        }
    }

    static Vector3 LocalPositionByIndex(Vector2Int index)
    {
        return new Vector3(10f * index.x, 0f, -10f * index.y);
    }

    public void StopBattle()
    {
        foreach (var cannon in cannons)
        {
            cannon.enabled = false;
        }
    }
}
