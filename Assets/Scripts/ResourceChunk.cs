using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceChunk : MonoBehaviour
{
    public Collider collider;
    public Rigidbody rigidbody;
    public ResourceManager.ResourceType resType { get;}
    public int amount;
    [SerializeField] private float nonCollectibleTime = 1f;
    [SerializeField] private int layer;

    private void Awake()
    {
        StartCoroutine(DelayedCollect());
    }

    IEnumerator DelayedCollect()
    {
        yield return new WaitForSeconds(nonCollectibleTime);
        gameObject.layer = layer;
    }

    public void OnCollect()
    {
        Destroy(gameObject);
    }
    public void Float()
    {
        collider.isTrigger = false;
    }
}
